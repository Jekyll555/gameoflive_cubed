  
enum dir_e {
  dirLeft,
  dirRight,
  dirDown,
  dirUp
};
  
enum state_e {
  golDead,
  golBirth,
  golLiving,
  golDying
};

typedef struct movePos_s {
  dir_e dir;
  int x;
  int y;
};

int _inRange(int x,int min,int max) {
  return ((x-min)*(x-max) <= 0);
}


int  _left(movePos_s *f)  {   if (f->x > 0)           { f->x -= 1; } else { f->x = sizeX-1;  }  }
int  _right(movePos_s *f) {   if (f->x < sizeX - 1)   { f->x += 1; } else { f->x = 0;        }  }
int  _down(movePos_s *f)  {   if (f->y < sizeY - 1)   { f->y += 1; } else { f->y = 0;        }  }
int  _up(movePos_s *f)    {   if (f->y > 0)           { f->y -= 1; } else { f->y = sizeY-1;  }  }

void NextField(movePos_s *f)
{
  //catch 3d cube exception coordinates
  if(        (f->dir ==  dirLeft) && (f->x ==  0) ) {
    if (_inRange(f->y,0,15)) {
      f->x =f->y;
      f->y =31;
      f->dir = dirDown;
    } else if (_inRange(f->y,16,31)) {
      f->y=f->y+16;
    } else if (_inRange(f->y,32,47)) {
      f->y=f->y-16;
    }
  } else if ((f->dir == dirRight) && (f->x == 15) ) {
    if (_inRange(f->y,0,15)) {
      f->x =f->y;
      f->y =0; //should become 47
      f->dir = dirUp;
    } else if (_inRange(f->y,16,31)) {
      f->y=f->y+16;
    } else if (_inRange(f->y,32,47)) {
      f->y=f->y-16;
    }      
  } else if ((f->dir ==    dirUp) && (f->y ==  0) ) {  
    f->y = 32;
  } else if ((f->dir ==  dirDown) && (f->y == 31) ) {  
    f->y = 47;
  } else if ((f->dir ==    dirUp) && (f->y == 32) ) {  
    f->y = f->x;
    f->x = 15;
    f->dir = dirRight;
  } else if ((f->dir ==  dirDown) && (f->y == 47) ) {  
    f->y = f->x;
    f->x = 0;  
    f->dir = dirLeft;
  }  
  
  if(f->dir == dirUp)            { _up(f);     } 
  else if (f->dir == dirDown)    { _down(f);   }
  else if (f->dir == dirLeft)    { _left(f);   }
  else if (f->dir == dirRight)   { _right(f);  }
}



dir_e setDirection(dir_e dir, dir_e turn) {
      dir_e retDir;
      //only using left and right to alter RELATIVE direction of movement (up / down will just advance in 'relative forward direction')
      if      ((dir ==    dirUp) && (turn ==  dirLeft))  {  retDir = dirLeft;      }
      else if ((dir ==    dirUp) && (turn == dirRight))  {  retDir = dirRight;     }
      else if ((dir ==  dirDown) && (turn ==  dirLeft))  {  retDir = dirRight;     }
      else if ((dir ==  dirDown) && (turn == dirRight))  {  retDir = dirLeft;      }
      else if ((dir ==  dirLeft) && (turn ==  dirLeft))  {  retDir = dirDown;      }
      else if ((dir ==  dirLeft) && (turn == dirRight))  {  retDir = dirUp;        }
      else if ((dir == dirRight) && (turn ==  dirLeft))  {  retDir = dirUp;        }
      else if ((dir == dirRight) && (turn == dirRight))  {  retDir = dirDown;      }

      return retDir;
 }

