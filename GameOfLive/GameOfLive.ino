/*****************************************************************
 *****************************************************************

  CONWAY'S GAME OF LIFE

* *****************************************************************
*******************************************************************/

//////////////////////////
//      LIBRARIES       //
//////////////////////////

#include <Adafruit_GFX.h>   // Core graphics library
#include <Adafruit_NeoMatrix.h>
#include <Adafruit_NeoPixel.h>

//////////////////////////
//       VARIABLES      //
//////////////////////////

//Definition of the pattern if you use the pattern initialization
char pattern_init[] =
//   ".........*,\
//    .......*.*,\
//    ......*.*,\
//    **...*..*...........**,\
//    **....*.*...........**,\
//    .......*.*,\
//    .........*!";

//".*.,..*,***!";   //glider

//"*,*,*!";
//"***!";
"*,**,**!";

#define Matrix_X 16
#define Matrix_Y 16
#define TileNum_X 1
#define TileNum_Y 3
#define LED_COUNT (Matrix_X * Matrix_Y * TileNum_X * TileNum_Y)

#define sizeX (Matrix_X * TileNum_X)
#define sizeY (Matrix_Y * TileNum_Y)

#define systemTickDelayMS 50

#include "LocatorHelper.h"
#include "getX.h"

state_e WORLD[sizeX][sizeY]; // Creation of the wordl
state_e WORLD_new[sizeX][sizeY]; // Creation of the wordl
int step_GOL; //used to know the generation

//////////////////////////
//       OBJECTS        //
//////////////////////////


//Definition of the LED Matrix Object
#define LedPin D2


Adafruit_NeoMatrix matrix(sizeX, sizeY, LedPin,
  NEO_MATRIX_TOP     + NEO_MATRIX_LEFT +
  NEO_MATRIX_COLUMNS + NEO_MATRIX_ZIGZAG,
  NEO_GRB            + NEO_KHZ800);

#include "PrintStuff.h"

void setSeed() {
//  randomSeed(analogRead(A0));
//  randomSeed(ESP.getCycleCount());
  randomSeed(RANDOM_REG32);
}

/*************************************************************************************************************/
//////////////////////////
//          SETUP       //
//////////////////////////
void setup() {
  Serial.begin(115200); //use to print the game on the serial monitor (to debug)

  pinMode(A0,INPUT);
  //Randomly initialazing the world for the first step
  setSeed();
  for (byte i = 0; i < sizeX; i++) {
    for (byte j = 0; j < sizeY; j++) {
      WORLD[i][j] = (state_e) random(0, 2); //todo
    }
  }

//  init_WORLD(); // Uncomment if you want to init with a specific pattern

  step_GOL = 0;
  matrix.begin();
  print_WORLD(); //Display the first generation

}


/*************************************************************************************************************/
//////////////////////////
//    LOOP FUNCTION     //
//////////////////////////
void loop() {
  if (step_GOL == 600) { // This if reboot the world after 60 generation to avoid static world
    step_GOL = 0;
    matrix.fillScreen(0);
    if(systemTickDelayMS) delay(systemTickDelayMS);
    //Randomly initialazing the world for the first step
    setSeed();
    for (byte i = 0; i < sizeX; i++) {
      for (byte j = 0; j < sizeY; j++) {
        WORLD[i][j] = (state_e) random(0, 2); //todo
      }
    }
  }
  //This double "for" is used to update the world to the next generation
  //The buffer state is written on the EEPROM Memory

  for (byte i = 0; i < sizeX; i++) {
    for (byte j = 0; j < sizeY; j++) {
      
        byte num_alive = getNumAlive(i,j);
        state_e state = WORLD[i][j];

        //RULE#1 if you are surrounded by 3 cells --> you live
        if (num_alive == 3) {
          WORLD_new[i][j] = golBirth;
        }
        //RULE#2 if you are surrounded by 2 cells --> you stay in your state
        else if (num_alive == 2) {
          if (state == golBirth) state = golLiving;
          if (state == golDying) state = golDead;
          WORLD_new[i][j] = state;
        }
        //RULE#3 otherwise you die from overpopulation or subpopulation
        else if ( chkL(WORLD_new[i][j]) ) {
          WORLD_new[i][j] = golDying;
        } else {
          WORLD_new[i][j] = golDead;
        }
      
    }
  }

  //Updating the World
  for (byte i = 0; i < sizeX; i++) {
    for (byte j = 0; j < sizeY; j++) {
      WORLD[i][j] = WORLD_new[i][j];
    }
  }

  //Displaying the world
  print_WORLD();
  
//print_WORLD_SERIAL();

  //Increasing the generation
  step_GOL++;
  
}

/*************************************************************************************************************/
//////////////////////////
//       FUNCTIONS      //
//////////////////////////


//This function is used to init the world with a know pattern
//It read . and * to convert them to 0 and 1.
//Inspired from life 1.05 format
// NB : this function needs improvment to center the pattern

void init_WORLD() {
  int k = 0;
  int column = 0, offsetc =0; //x
  int row = 0, offsetr =15;  //y

  for (byte i = 0; i < sizeX; i++) {
    for (byte j = 0; j < sizeY; j++) {
      WORLD[i][j] = golDead;
    }
  }
  
  while (pattern_init[k] != '!') { //end
    if (pattern_init[k] == ',') { //next row
      row++;
      k++;
      column = 0;
    }
    else if (pattern_init[k] == '.') {
      WORLD[column+offsetc][row+offsetr] = golDead;
      k++;
      column ++;
    }
    else  {
      WORLD[column+offsetc][row+offsetr] = golLiving;
      k++;
      column ++;
    }
  }
}
