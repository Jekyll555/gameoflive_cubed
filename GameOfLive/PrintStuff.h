

void clearscreen() {
  for (int i = 0; i < 10; i++) {
    Serial.println("\n\n\n\n");
  }
}

byte chkL(state_e state) { //check Living
  return (state == golLiving || state == golBirth);
}

// PRINT THE WORLD
void print_WORLD()
{
  for (byte j = 0; j < sizeY; j++) {
    for (byte i = 0; i < sizeX; i++) {
      if (WORLD[i][j] == golDead) {
        matrix.setPixelColor(getX(i,j), 0, 0, 0);
      }
      else if (WORLD[i][j] == golBirth) {
        matrix.setPixelColor(getX(i,j), 30, 100, 30);
      }
      else if (WORLD[i][j] == golLiving) {
        matrix.setPixelColor(getX(i,j), 100,150,20 );//30, 80, 20
      }
      else if (WORLD[i][j] == golDying) {
        matrix.setPixelColor(getX(i,j), 50, 40, 20);
      } else {
        matrix.setPixelColor(getX(i,j), 0, 0, 0);
      }
       
//      Serial.print(" got Num... ");
//      Serial.print(" at x ");
//      Serial.print(i);
//      Serial.print(" y ");
//      Serial.println(j);

      
    }
  }
  matrix.show();
  if (systemTickDelayMS) delay(systemTickDelayMS);
}

//Those two function are used to display the world on the serial monitor
//Not beautiful but useful to debug

void print_WORLD_SERIAL()
{
  clearscreen();
  Serial.print("Step = "); Serial.println(step_GOL);
  for (int i = 0; i < sizeX; i++) {
    for (int j = 0; j < sizeY; j++) {
      if (WORLD[i][j] == 0) {
        //Serial.print(".");
        Serial.print("_");
      }
      else
      {
        Serial.print("O");
        //Serial.print(" ");
      }
    }
    Serial.println("");
  }
  Serial.println("");

}

byte getNumAlive (int i, int j) {
  byte ret=0;
  
  if (((i%Matrix_X) == 0 ) ||
      ((i%Matrix_X) == (Matrix_X-1)) ||
      ((j%Matrix_Y) == 0 ) || 
      ((j%Matrix_Y) == (Matrix_Y-1)))
  {
    movePos_s f;
    f.x=i; f.y=j;
    f.dir=dirLeft; //some default where it doesnt matter

    //on the edges we just have 7 neighbors!
    if        (((i%Matrix_X) == 0 ) &&            ((j%Matrix_Y) == 0 )) {
      f.dir=dirDown; 
    } else if (((i%Matrix_X) == 0 ) &&            ((j%Matrix_Y) == (Matrix_Y-1))) {
      f.dir=dirRight; 
    } else if (((i%Matrix_X) == (Matrix_X-1)) &&  ((j%Matrix_Y) == 0 )) {
      f.dir=dirLeft; 
    } else if (((i%Matrix_X) == (Matrix_X-1)) &&  ((j%Matrix_Y) == (Matrix_Y-1))) {
      f.dir=dirUp;
    }
    
    NextField(&f);
     if(chkL(WORLD[f.x][f.y])) ret = ret+1;

    f.dir=setDirection(f.dir, dirRight);   
    NextField(&f);
    if(chkL(WORLD[f.x][f.y])) ret = ret+1;

    
    f.dir=setDirection(f.dir, dirRight);   
    NextField(&f);
    if(chkL(WORLD[f.x][f.y])) ret = ret+1;
    
    NextField(&f);
    if(chkL(WORLD[f.x][f.y])) ret = ret+1;

    //skip this in case of edge 
    if ((((i%Matrix_X) == 0 ) ||
         ((i%Matrix_X) == (Matrix_X-1))) &&
        (((j%Matrix_Y) == 0 ) || 
         ((j%Matrix_Y) == (Matrix_Y-1)))) {
          //skip 
    } else {
      f.dir=setDirection(f.dir, dirRight);   
      NextField(&f);
      if(chkL(WORLD[f.x][f.y])) ret = ret+1;
    }
    
    NextField(&f);
    if(chkL(WORLD[f.x][f.y])) ret = ret+1;
    
    f.dir=setDirection(f.dir, dirRight);   
    NextField(&f);
    if(chkL(WORLD[f.x][f.y])) ret = ret+1;

    NextField(&f);
    if(chkL(WORLD[f.x][f.y])) ret = ret+1;

//    if(ret>0) {
//      Serial.print(" got Num ");
//      Serial.print(ret);
//      Serial.print(" at x ");
//      Serial.print(i);
//      Serial.print(" y ");
//      Serial.println(j);
//    }

  } else{
    ret = chkL(WORLD[i - 1][j - 1]) + 
          chkL(WORLD[i - 1][j]    ) + 
          chkL(WORLD[i - 1][j + 1]) +  
          chkL(WORLD[i]    [j - 1]) + 
          chkL(WORLD[i]    [j + 1]) + 
          chkL(WORLD[i + 1][j - 1]) + 
          chkL(WORLD[i + 1][j]    ) + 
          chkL(WORLD[i + 1][j + 1]);
  }
  return ret;
}

