


int getX(byte x, byte y) {
  int ret;
    if(((x*Matrix_X)/16)%2==0) {
        ret= (x * TileNum_X * Matrix_X) + y%Matrix_Y + (y/Matrix_Y)* Matrix_Y * Matrix_X;
    } else {
        ret= (x * TileNum_X * Matrix_X) + (Matrix_Y-1-y%Matrix_Y) + (y/Matrix_Y)* Matrix_Y * Matrix_X;
    }

    //exchange tile 2 and 3
    if(ret< (2*Matrix_Y * Matrix_X) &&
       ret>=(  Matrix_Y * Matrix_X) ) {
        ret += (Matrix_Y * Matrix_X);
    } else 
    if(ret< (3*Matrix_Y * Matrix_X) &&
       ret>=(2*Matrix_Y * Matrix_X) ) {
        ret -= (Matrix_Y * Matrix_X);
    }
    return ret;
}



